<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelid
 * @property integer $statusid
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'levelid', 'statusid'], 'required'],
            [['id', 'levelid', 'statusid'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelid' => 'Levelid',
            'statusid' => 'Statusid',
        ];
    }
	
	
}
