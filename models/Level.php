<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
	

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	public static function getLevels()
	{
		$allLevels = self::find()->all();
		$allLevelsArray = ArrayHelper::
					map($allLevels, 'id', 'name');
		return $allLevelsArray;						
	}
	
	public static function getLevelsWithAlllevels()
	{
		$allLevels = self::getLevels();
		$allLevels[-1] = 'All Statuses';
		$allLevels = array_reverse ( $allLevels, true );
		return $allLevels;	
	}		
}
